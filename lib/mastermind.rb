

class Code
  PEGS = {
    "B" => :blue,
    "G" => :green,
    "O" => :orange,
    "P" => :purple,
    "R" => :red,
    "Y" => :yellow
  }
  CODE_LENGTH = 4  # Length of code that you must guess correctly
  attr_reader :pegs

  def initialize(pegs)
    @pegs = pegs
  end

  def self.random
    code = []
    pegs = CODE_LENGTH.times { code << PEGS.values.sample }
    Code.new(code)
  end

  def [](position)
    @pegs[position]
  end

  # Needs self to run it on the Object class itself i.e. Code.parse("BBBB")
  def self.parse(user_guess)
    if user_guess.upcase.chars.all? { |color_char| PEGS.keys.include?(color_char) }
      parsed_pegs = []
      user_guess.upcase.chars.each { |color_char| parsed_pegs << PEGS[color_char] }
      Code.new(parsed_pegs)
    else
      raise "These are invalid colors. Cannot be parsed."
    end
  end

  # Doesn't need self in front if running on instances of the objects
  def exact_matches(other_code)
    @pegs.each_with_index.count { |peg, indx| other_code.pegs[indx] == peg }
  end

  def near_matches(other_code)
    exact_match = self.exact_matches(other_code)
    helper = Hash.new(0)
    @pegs.each { |peg| helper[peg] += 1 }

    count_match = other_code.pegs.count do |peg|
      if helper[peg] > 0
        helper[peg] -= 1
        true
      end
    end
    count_match - exact_match
  end

  def ==(code)
    return false unless code.is_a?(Code)
    @pegs == code.pegs
  end
end


class Game
  TURNS = 10
  attr_reader :secret_code

  def initialize(secret_code=Code.random)
    @secret_code = secret_code
    puts "Welcome to the mastermind game."
    puts "You must correctly guess the secret code of length #{Code::CODE_LENGTH}."
    puts "The possible colors are Blue, Green, Orange, Purple, Red, and Yellow."
  end

  def play
    TURNS.times do
      guess = get_guess

      if guess == @secret_code
        puts "Congrats! You have solved the game! :)"
        return
      end

      display_matches(guess)
    end
    puts "GG. Better luck next time :("
    puts "The correct answer was #{@secret_code.pegs}"
  end

  def get_guess
    puts "Input your guess."
    begin
      Code.parse(gets.chomp)
    rescue
      puts "These are invalid colors. Input your guess like 'GOPB'; duplicates are valid."
      retry
    end
  end

  def display_matches(code)
    puts "Exact matches: #{@secret_code.exact_matches(code)}"
    puts "Near matches: #{@secret_code.near_matches(code)}"
  end
end
